import pexpect

if __name__ == '__main__':
    proc = pexpect.spawn('./a.out', echo=False)

    proc.expect("Give your default input")
    proc.sendline(f"a=10\nc=3\nb=2\na=1\nSTOP\n")
    proc.expect("Give your current input")
    print("OUTPUT:")
    print(proc.before.decode("utf-8"))

    proc.sendline(f"b=10\nSTOP\n")
    proc.expect("Give your current input")
    print("OUTPUT:")
    print(proc.before.decode("utf-8"))

    proc.sendline(f"a=15\nSTOP\n")
    proc.expect("Give your current input")
    print("OUTPUT:")
    print(proc.before.decode("utf-8"))
