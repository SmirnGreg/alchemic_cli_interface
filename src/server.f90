module Constants
    implicit none
    integer, PARAMETER :: DP = KIND(1.0d0)                   !Is used in the most code.
    real(DP), parameter :: hP = 6.626070040e-27_DP          !Planck constant           [erg s]
    real(DP), parameter :: pi = 3.14159265358979e0_DP       !Pi
    real(DP), parameter :: mH = 1.672621898e-24_DP          !Proton mass               [g]
    real(DP), parameter :: ep = 4.80320425e-10_DP           !Elementary charge         [statC]
    real(DP), parameter :: kB = 1.38064852e-16_DP           !Boltzmann constant        [erg K-1]
    real(DP), parameter :: eV = 1.6021766208e-12_DP         !Electron volt             [erg]
    real(DP), parameter :: year = 3.15576e7_DP                !Julian year               [s]
    real(DP), parameter :: Myr = 1e6_DP * year                 !10^6 Julian years         [s]
    real(DP), parameter :: Gg = 6.67408e-8_DP               !Graviatational constant   [cm3 g-1 s-2]
    real(DP), parameter :: MEarth = 3.986004e20_DP / Gg              !Earth mass
end module Constants

module class_ChemicalReaction
    use Constants, only : DP
    implicit none
    private
    integer, parameter, public :: species_name_length = 12
    type, public :: ChemicalReaction
        integer :: index, rtype = 0
        character(len = species_name_length) :: &
                reactant_1, reactant_2, &
                product_1, product_2, product_3, product_4, product_5, &
                type = "NONE"
        integer :: ir1, ir2, ip1, ip2, ip3, ip4, ip5
        real(DP) :: alpha = 0.0_DP, beta = 0.0_DP, gamma = 0.0_DP
        real(DP) :: rate
    contains
        procedure :: calculate_rtype
    end type ChemicalReaction
contains
    subroutine calculate_rtype(this)
        class (ChemicalReaction), intent(inout) :: this
        integer :: rtype
        character (len = species_name_length) :: r1, r2
        r1 = this%reactant_1
        r2 = this%reactant_2

        rtype = 1 !Two body molecule/ion - molecule/ion reactions
        if (trim(r2) == 'CRP')                                     rtype = 2 !CR and XR ionization
        if (trim(r2) == 'PHOTON')                                  rtype = 3 !Photoionization/photodissociation
        if (trim(r1) == 'H2' .AND. trim(r2) == 'PHOTON') rtype = 4 !Special case of rtype=3, for self-shielded H2
        if (trim(r1) == 'CO' .AND. trim(r2) == 'PHOTON') rtype = 5 !Special case of rtype=3, for self-shielded CO
        if (trim(r2) == 'CRPHOT')                                  rtype = 6
        if (trim(r2) == 'G-')                                      rtype = 7
        if (trim(r2) == 'G0')                                      rtype = 8
        if (trim(r1) == 'G0')                                      rtype = 9
        if (trim(r1) == 'G+')                                      rtype = 10
        if (trim(r2) == 'FREEZE')                                  rtype = 11
        if (trim(r2) == 'DESORB')                                  rtype = 12
        if (r1(1:1) == 'g' .AND. r2(1:1) == 'g')                           rtype = 13
        if (r1(1:1) == 'G' .AND. TRIM(r2) == 'PHOTON')                     rtype = 14 !Photoionization of dust grains
        if (r1(1:1) == 'a' .AND. TRIM(r2) == 'ASTEROID')                   rtype = 15 !Release of fresh ice in asteroid collisions
        if (trim(r2) == "XRAYS")                                           rtype = 16

        this%rtype = rtype
    end subroutine calculate_rtype
end module class_ChemicalReaction

module class_ChemicalNetwork
    use class_ChemicalReaction, only : ChemicalReaction, species_name_length
    use Constants, only : DP
    implicit none
    private

    type, public :: ChemicalNetwork
        character(len = 128) :: path_reactions = ""
        character(len = 128) :: path_species = ""
        logical :: is_read = .false.
        integer :: number_of_reactions=0, number_of_species=0
        type(ChemicalReaction), allocatable :: reactions(:)
        character(len = species_name_length), allocatable :: species(:)
        real(DP), allocatable :: species_weight(:)
        character(len = 128) :: format="*"
    contains
        procedure :: read => read_network
        procedure :: report
        procedure :: find_species_index
    end type ChemicalNetwork

contains
    subroutine find_species_index(this, i)
        class (ChemicalNetwork), intent(inout) :: this
        integer :: i
        this%reactions(i)%ir1 = FINDLOC(this%species, value = this%reactions(i)%reactant_1, dim = 1)
        this%reactions(i)%ir2 = FINDLOC(this%species, value = this%reactions(i)%reactant_2, dim = 1)

        this%reactions(i)%ip1 = FINDLOC(this%species, value = this%reactions(i)%product_1, dim = 1)
        this%reactions(i)%ip2 = FINDLOC(this%species, value = this%reactions(i)%product_2, dim = 1)
        this%reactions(i)%ip3 = FINDLOC(this%species, value = this%reactions(i)%product_3, dim = 1)
        this%reactions(i)%ip4 = FINDLOC(this%species, value = this%reactions(i)%product_4, dim = 1)
        this%reactions(i)%ip5 = FINDLOC(this%species, value = this%reactions(i)%product_5, dim = 1)
    end subroutine find_species_index

    REAL(DP) PURE FUNCTION aweight(xin)
        IMPLICIT NONE

        CHARACTER(len = species_name_length), intent(in) :: xin
        CHARACTER(len = species_name_length) :: x

        ! Local variable(s):
        INTEGER i, first, last, n
        REAL(DP) :: w1(8), w2(6)
        CHARACTER(len = 1) :: s1(8), tmp1, multi
        CHARACTER(len = 2) :: s2(6), tmp2
        LOGICAL cond
        ! Initialization of the output:
        aweight = 0._DP

        ! Initialization of atom's names and their weights:
        s1(1) = 'H'   ;    w1(1) = 1.00790E-00_DP
        s1(2) = 'C'   ;    w1(2) = 1.20110E+01_DP
        s1(3) = 'N'   ;    w1(3) = 1.40067E+01_DP
        s1(4) = 'O'   ;    w1(4) = 1.59994E+01_DP
        s1(5) = 'S'   ;    w1(5) = 3.20660E+01_DP
        s1(6) = 'P'   ;    w1(6) = 3.09738E+01_DP
        s1(7) = 'D'   ;    w1(7) = 2.0_DP * w1(1)
        s1(8) = 'F'   ;    w1(8) = 1.90000E+01_DP
        s2(1) = 'He'  ;    w2(1) = 4.00260E-00_DP
        s2(2) = 'Fe'  ;    w2(2) = 5.58470E+01_DP
        s2(3) = 'Si'  ;    w2(3) = 2.80855E+01_DP
        s2(4) = 'Na'  ;    w2(4) = 2.29898E+01_DP
        s2(5) = 'Mg'  ;    w2(5) = 2.43050E+01_DP
        s2(6) = 'Cl'  ;    w2(6) = 3.54527E+01_DP



        ! Initialization of borders of the species name:
        !CALL len_tri2(x,10,last)
        x = TRIM(xin)
        last = LEN_TRIM(x)

        ! Special species:
        IF (x(1:2)=='e-') then
            aweight = 1.00790_DP / 1836._DP
            return
        endif
        IF (x(1:2)=='G0' .OR. x(1:2)=='G-' .OR. x(1:2)=='G+') RETURN

        ! First consider a special case of C10-bearing species:
        IF ((x(1:3)=='C10').or.(x(2:4)=='C10')) THEN
            aweight = 120.11e+00_DP
            RETURN
        END IF

        ! Other cases:
        first = 1

        ! Adsorbed species:
        IF (x(first:first)=='g'.or.x(first:first)=='a') THEN
            first = 2
            ! Ions:
        ELSE IF (x(last:last)=='+') THEN
            last = last - 1
        ELSE IF (x(last:last)=='-') THEN
            last = last - 1
        END IF

        ! Divide species onto atomic constituents:
        !10 CONTINUE
        do while(.true.)
            IF (first > last) EXIT ! checked the species,  exit,

            ! If it is not a last character in the species:
            IF ((last - first) >= 1) THEN
                tmp1 = x(first:first)
                tmp2 = x(first:(first + 1))
                cond = .true.

                ! Start a search among two-letter atoms:
                DO i = 1, 6
                    IF (tmp2==s2(i)) THEN
                        aweight = aweight + w2(i)
                        first = first + 2
                        cond = .false.
                    END IF
                END DO

                ! Start a search among one-letter atoms:
                IF (cond) THEN
                    DO i = 1, 8
                        IF (tmp1==s1(i)) THEN
                            multi = tmp2(2:2)
                            ! Find a possible multiplicators:
                            n = 1
                            IF (multi=='2') THEN
                                n = 2
                                first = first + 2
                            ELSE IF (multi=='3') THEN
                                n = 3
                                first = first + 2
                            ELSE IF (multi=='4') THEN
                                n = 4
                                first = first + 2
                            ELSE IF (multi=='5') THEN
                                n = 5
                                first = first + 2
                            ELSE IF (multi=='6') THEN
                                n = 6
                                first = first + 2
                            ELSE IF (multi=='7') THEN
                                n = 7
                                first = first + 2
                            ELSE IF (multi=='8') THEN
                                n = 8
                                first = first + 2
                            ELSE IF (multi=='9') THEN
                                n = 9
                                first = first + 2
                                ! There is no a multiplicator:
                            ELSE
                                first = first + 1
                            END IF
                            aweight = aweight + n * w1(i)
                        END IF
                    END DO
                END IF

                ! Checking the last character:
            ELSE
                DO i = 1, 8
                    IF (x(first:last)==s1(i)) THEN
                        aweight = aweight + w1(i)
                        first = first + 1
                    END IF
                END DO
            END IF

        ENDDO

    END FUNCTION aweight


    subroutine read_network(this, path_reactions)
        class(ChemicalNetwork), intent(out) :: this
        character(len = *), intent(in) :: path_reactions

        integer :: i, fileunit

        this%path_reactions = path_reactions

        open(newunit = fileunit, file = path_reactions)
        read(fileunit, *) this%number_of_species, this%format
        allocate(this%species(this%number_of_species))
        allocate(this%species_weight(this%number_of_species))

        do i = 1, this%number_of_species
            read(fileunit, "(A)") this%species(i)
            this%species_weight(i) = aweight(this%species(i))
        end do

        read(fileunit, *) this%number_of_reactions
        allocate(this%reactions(this%number_of_reactions))
        do i = 1, this%number_of_reactions
            read(fileunit, this%format) &
                    this%reactions(i)%index, &
                    this%reactions(i)%reactant_1, this%reactions(i)%reactant_2, &
                    this%reactions(i)%product_1, this%reactions(i)%product_2, this%reactions(i)%product_3, &
                    this%reactions(i)%product_4, this%reactions(i)%product_5, &
                    this%reactions(i)%alpha, this%reactions(i)%beta, this%reactions(i)%gamma, &
                    this%reactions(i)%type
            call this%reactions(i)%calculate_rtype()
            call this%find_species_index(i)
        end do
        close(fileunit)
        this%is_read = .true.
    end subroutine read_network

    subroutine report(this)
        class(ChemicalNetwork), intent(in) :: this

        integer :: i

        write(*, *) "ALCHEMIC chemical network"
        write(*, *) "Number of reactions: ", this%number_of_reactions
        do i = 1, this%number_of_reactions
            write(*, *) &
                    this%reactions(i)%rate, &
                    this%reactions(i)%index, ": ", &
                    this%reactions(i)%reactant_1, " + ", this%reactions(i)%reactant_2, " = ", &
                    this%reactions(i)%product_1, " + ", this%reactions(i)%product_2, " + ", &
                    this%reactions(i)%product_3, " + ", this%reactions(i)%product_4, " + ", &
                    this%reactions(i)%product_5, ". ", &
                    this%reactions(i)%alpha, this%reactions(i)%beta, this%reactions(i)%gamma, ". ", &
                    this%reactions(i)%type
        end do
        write(*, *) "Format: ", this%format
    end subroutine report
end module class_ChemicalNetwork

module class_Alchemic
    use class_ChemicalNetwork, only : ChemicalNetwork
    use class_ChemicalReaction, only : species_name_length, ChemicalReaction
    use DVODE_F90_M
    use Constants, only : DP, hP, pi, mH, ep, kB, eV, year, Myr, Gg, MEarth

    use, intrinsic :: iso_fortran_env, only : stdin => input_unit, &
            stdout => output_unit, &
            stderr => error_unit
    implicit none
    private

    integer, public, parameter :: parameter_length = 128
    integer, public, parameter :: parameter_name_length = 64
    character(len = parameter_name_length), dimension (*), parameter :: param_names = [&
            character(len = parameter_name_length) :: &
                    "Radiation field type", "Fraction of UV in LyA", "UV photodesorption yield", "Ambient ISRF", &
                    "CRP ionization rate scaling factor", "SLR ionization rate", "desorption energies scaling factor", &
                    "Xray stellar luminosity scaling factor", "UV stellar luminosity scaling factor", &

                    "Gas density", "Gas temperature", "Dust temperature", "Dust radius", "Dust to gas mass ratio", "UV field", &
                    "Stellar Av", "Interstellar Av", "CR ionization rate", "XRay ionization rate", &
                    "Interstellar H2 self-shielding", "Interstellar CO self-shielding", &

                    "Stellar H2 self-shielding", "Stellar CO self-shielding", "Scale height", &
                    "Evolution time", "First time step", "Number of time steps", &
                    "Relative tolerance", "Absolute tolerance", &

                    "Initial abundance file", &
                    "Dust grain density", "Desorption/diffusion", "Desorption energy", "RATE/MRAT", "Surface site density", &
                    "Stochastic surface population limit", "Products remain on surface", "Sticking probability", "Dust albedo UV", &

                    "Tunneling through barrier", "Tunneling through site walls", &

                    "Chemical network file" &
            ]

    integer, parameter :: n_parameters = size(param_names)
    character(len = parameter_length), dimension (n_parameters) :: default_values = [&
            character(len = parameter_length) :: &
                    "ISRF", "0.0d0", "1.0d-5", "1.0d0", &
                    "1.0d0", "6.5d-19", "1.0d0", &
                    "1.0d0", "1.0d0", &

                    "1.0d-17", "10.", "10.", "1.0d-5", "1.0d-2", "1.0d0", &
                    "1.0d100", "1.0d100", "1.3d-17", "1.0d-100", &
                    "1.0d0", "1.0d0", &

                    "1.0d-100", "1.0d-100", "NONE", &
                    "1.0d6", "1.0d0", "5", &
                    "1.0d-4", "1.0d-15", &

                    "3abunds.inp", &
                    "3.0d0", "0.4d0", "OBIH", "RATE", "2.0d14", &
                    "1.0d5", "0.99d0", "1.0", "0.5", &

                    "FALSE", "FALSE", &

                    "Chemical_Network_ALCHEMIC_KIDAbench_reduced.dat" &
            ]
    type, public :: Alchemic
        character(len = parameter_length) :: network_path
        character(len = parameter_length), dimension (n_parameters) :: param_values = ""
        type(ChemicalNetwork) :: network
        real(DP), allocatable :: abundances(:, :), initial_abundances(:), times(:)
    contains
        procedure :: from_stdin
        procedure :: run
        procedure :: report
        procedure :: report_abundances
        procedure :: report_defaults
        procedure, pass :: get
        procedure :: set
        procedure :: set_default
        procedure, pass :: get_default
        procedure :: set_default_from_current
        procedure :: set_current_from_default
        procedure :: read_network
        procedure, pass :: findindex
        procedure, pass :: get_real
        procedure, pass :: get_int
        procedure, pass :: get_bool
        procedure :: calculate_rates
        procedure :: read_initial_abundances
        procedure :: run_dvode_solver_sparse
    end type Alchemic

contains
    subroutine calculate_rates(this)
        class(Alchemic), intent(inout) :: this
        real(DP) :: stick_probability_hydrogen, stick_probability, coulomb_attraction, tgas, tdust, dust_numdens
        real(DP) :: ionization_rate_crp, ionization_rate_xray
        real(DP) :: av_interstellar, av_stellar, uv_field_stellar, uv_field_interstellar
        real(DP) :: field_h2_is, field_h2_s, field_co_is, field_co_s
        real(DP) :: ss_h2_is, ss_h2_s, ss_co_is, ss_co_s
        real(DP) :: uv_photodesorption
        real(DP) :: dust_to_gas, gas_density, grain_radius, monomer_density, sads
        real(DP) :: velocity_thermal, dust_albedo_uv, site_density, rdratio
        real(DP) :: anu2, anu1, ak_therm, ak_photo, ak_crp, akbar, amur, delta, tcrp, ebed, rdiff1, rdiff2
        logical :: tunneling_barrier, tunneling_hop
        integer :: i

        tgas = this%get_real("Gas temperature")
        gas_density = this%get_real("Gas density")
        tdust = this%get_real("Dust temperature")
        dust_to_gas = this%get_real("Dust to gas mass ratio")
        monomer_density = this%get_real("Dust grain density")
        grain_radius = this%get_real("Dust radius")

        av_stellar = this%get_real("Stellar Av")
        av_interstellar = this%get_real("Interstellar Av")
        uv_field_stellar = this%get_real("UV field")
        uv_field_interstellar = this%get_real("Ambient ISRF")

        ss_h2_s = this%get_real("Stellar H2 self-shielding")
        ss_h2_is = this%get_real("Interstellar H2 self-shielding")

        ss_co_s = this%get_real("Stellar CO self-shielding")
        ss_co_is = this%get_real("Interstellar CO self-shielding")

        field_h2_is = uv_field_interstellar * exp(-2.5_DP *  av_interstellar)
        field_h2_s = uv_field_stellar * exp(-2.5_DP *  av_stellar)
        field_co_is = uv_field_interstellar
        field_co_s = uv_field_stellar

        stick_probability = this%get_real("Sticking probability")
        dust_albedo_uv = this%get_real("Dust albedo UV")

        uv_photodesorption = this%get_real("UV photodesorption yield")

        site_density = this%get_real("Surface site density")

        rdratio = this%get_real("Products remain on surface")
        ebed = this%get_real("Desorption/diffusion")

        tunneling_hop = this%get_bool("Tunneling through site walls")
        tunneling_barrier = this%get_bool("Tunneling through barrier")

        ionization_rate_crp = this%get_real("CR ionization rate") + this%get_real("SLR ionization rate")

        sads = 4 * pi * grain_radius * grain_radius * site_density
        delta = sqrt(1.0_DP / pi / site_density)
        dust_numdens = gas_density * dust_to_gas / (4._DP * pi * grain_radius**3 * monomer_density / 3._DP)



        !Hollenbach & McKee 1979, eq. 3.7:
        !stick_hydrogen = 1._DP / (1._DP+4e-2_DP*sqrt(Tdust+Tdust)+2e-3_DP*Tdust+8e-6_DP*Tdust*Tdust)
        stick_probability_hydrogen = 1._DP / (1._DP + 0.4_DP * sqrt(tgas / 100._DP + tdust / 100._DP) &
                + 0.2_DP * tgas / 100._DP + 0.08_DP * (tgas / 100._DP)**2)

        !Calculation of the long-range Coulomb attraction for ions:
        coulomb_attraction = 1._DP + 1.671e-3_DP / grain_radius / tgas
        do i = 1, this%network%number_of_reactions
            this%network%reactions(i)%rate = 0.0_DP
            SELECT CASE (this%network%reactions(i)%rtype)
            CASE (1) !Two-body molecule(ion) - molecule(ion) gas-phase reaction
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * &
                        (tgas / 300._DP)**this%network%reactions(i)%beta * &
                        exp(-this%network%reactions(i)%gamma / tgas)
            CASE (2) !Cosmic ray ionization reaction (CRP)
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * ionization_rate_crp !SIC total or CR only? DIMA says that in 2 and 6 CR+XRay, but not in DESORB (who knows?)
            CASE (16) !XRay ionization reaction (XRAYS)
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * ionization_rate_xray
            CASE (3) !Photoionization reaction (PHOTON)
                !            if(this%network%reactions(i)%rate.lt.0._DP) then !No precalculated rates for this photoreaction.
                !                 if(this%network%reactions(i)%gamma.lt.2.0_DP) then !A
                !                     this%network%reactions(i)%rate=Gfactor_Visual(ircur,izcur)*this%network%reactions(i)%alpha
                !                 else
                !                     this%network%reactions(i)%rate=Gfactor_UV(ircur,izcur)*this%network%reactions(i)%alpha
                !                 endif
                !            endif
                !Approach A is different from the next approach:
                !this%network%reactions(i)%alpha* EXP(-this%network%reactions(i)%gamma*AV_true(ircur,izcur) )*Gfactor_UV(ircur,nzp)
                !Approach A was adopted from Dmitry Wiebe's procedure atten.f90 and PhotoRates Module make use of it also.
                this%network%reactions(i)%rate = &
                        this%network%reactions(i)%alpha * EXP(-this%network%reactions(i)%gamma * av_stellar) * uv_field_stellar & !note, Gfactor_UV is taken at the upper cell
                                + this%network%reactions(i)%alpha &
                                * EXP(-this%network%reactions(i)%gamma * av_interstellar) * uv_field_interstellar

            CASE (4) !Photodissociation, H2 self-shielded
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha &
                        * (field_h2_is * ss_h2_is + field_h2_s * ss_h2_s)
            CASE (5) !Photoionization, CO self-shielded
                !                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * EXP(-this%network%reactions(i)%gamma * AV_from_Gfactor) * Gfactor_UV * CO_shield_coeff
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha &
                        * (field_co_is * ss_co_is + field_co_s * ss_co_s)
                !                write(*,*) "CO photodiss rate",this%network%reactions(i)%rate,CO_shield_coeff
            CASE (6) !Cosmic ray-induced photoreaction (CRPHOT)
                !alpha in CRPHOT reactions in currently used networks includes already albedo_UV=0.5 within.
                !If you want to consider another albedo_UV for ANDES1.0,ALCHEMIC,ALCHEMIC_reduced, uncomment the tail
                !Introducing new network for alpha with no albedo_UV, add this tail instead !/(1._DP-albedo_UV)

                !########### _Total or _CR ??
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * (ionization_rate_crp + ionization_rate_xray) !* (1._DP-0.5_DP)/(1._DP-albedo_UV)
            CASE (7) !Ions plus negatively charged grains
                velocity_thermal = sqrt(8._DP * kB * tgas / pi / this%network%species_weight(this%network%reactions(i)%ir1) / mH)
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * pi * grain_radius * grain_radius &
                        * stick_probability * coulomb_attraction * velocity_thermal
            CASE (8) !Ions plus neutral grains
                velocity_thermal = sqrt(8._DP * kB * tgas / pi / this%network%species_weight(this%network%reactions(i)%ir1) / mH)
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha * pi * grain_radius * grain_radius &
                        * stick_probability * velocity_thermal
            CASE (9) !Collisions of the electrons with the neutral grains
                velocity_thermal = sqrt(8._DP * kB * Tgas / pi / (mH / 1836._DP))
                this%network%reactions(i)%rate = pi * grain_radius * grain_radius * velocity_thermal &
                        * 1.329_DP * EXP(-Tgas / 20._DP)
            CASE (10) !Collisions of the electrons with the positively charged grains
                velocity_thermal = sqrt(8._DP * kB * Tgas / pi / (mH / 1836._DP))
                this%network%reactions(i)%rate = pi * grain_radius * grain_radius * velocity_thermal &
                        * coulomb_attraction * 1.329_DP * EXP(-Tgas / 20._DP)
            CASE (11) !Accretion (FREEZE)
                velocity_thermal = sqrt(8._DP * kB * tgas / pi / this%network%species_weight(this%network%reactions(i)%ir1) / mH)
                this%network%reactions(i)%rate = this%network%reactions(i)%alpha &
                        * pi * grain_radius * grain_radius * velocity_thermal * stick_probability * dust_numdens
                if (this%network%reactions(i)%reactant_1=='H') this%network%reactions(i)%rate = this%network%reactions(i)%alpha &
                        * pi * grain_radius * grain_radius * velocity_thermal * stick_probability_hydrogen * dust_numdens

                if (this%network%reactions(i)%reactant_1=='H2') this%network%reactions(i)%rate = 0._DP  !No H2 freezout
                if (this%network%reactions(i)%reactant_1=='pH2') this%network%reactions(i)%rate = 0._DP !No H2 freezout
                if (this%network%reactions(i)%reactant_1=='oH2') this%network%reactions(i)%rate = 0._DP !No H2 freezout

                !if (tdust>Tdust_no_surf_chem) this%network%reactions(i)%rate = 0._DP !No freeze-out if temperature is high
                !if (uv_field_stellar>Gfact_no_surf_chem) this%network%reactions(i)%rate = 0._DP !No freeze-out if radiation is high
            CASE (12) !Desorption (DESORB)
                ! Photodesorption: Walmsley ea 1999
                ak_photo = uv_photodesorption * 2e8_DP * delta * delta / 4._DP &
                        * (uv_field_stellar * exp(-2.0_DP * av_stellar) + uv_field_interstellar * exp(-2.0_DP * av_interstellar) &
                        + 9.6e2_DP * ionization_rate_crp / 1e-17_DP / (1._DP - dust_albedo_uv))

                ! Thermal desorption:
                anu1 = sqrt(2._DP * site_density * kB / mH * this%network%reactions(i)%gamma / pi / pi / &
                        this%network%species_weight(this%network%reactions(i)%ir1))
                ak_therm = anu1 &
                        * EXP(-this%network%reactions(i)%gamma / Tdust)
                ! CRP desorption: Le'ger et al. 1985 (CRP heating of 1000A grain, Eq. 1a)
                tcrp = (4.36e5_DP + Tdust**3)**(0.33333333333333333_DP)
                ak_crp = 2.431e-2_DP * ionization_rate_crp * anu1 * EXP(-this%network%reactions(i)%gamma / tcrp) !!!SIC what is needed here, CR or Total?
                write(*, *) i, this%network%reactions(i)%index, ak_photo, ak_therm, ak_crp
                this%network%reactions(i)%rate = ak_photo + ak_therm + ak_crp
            CASE (13) !Surface two-body reaction
                ! Compute their vibrational frequencies:
                ! if (this%network%reactions(i)%reactant_1=='gH        '.or.this%network%reactions(i)%reactant_1=='gH2       ') ebed=0.85
                anu1 = sqrt(2._DP * site_density * kB / mH * this%network%reactions(i)%gamma / pi / pi / &
                        this%network%species_weight(this%network%reactions(i)%ir1))
                anu2 = sqrt(2._DP * site_density * kB / mH * this%network%reactions(i)%gamma / pi / pi / &
                        this%network%species_weight(this%network%reactions(i)%ir2))
                ! Compute diffusion rates of the reactants (thermal hopping and quantum tunneling for the model of HHL 1992):
                rdiff1 = anu1 / (4._DP * pi * grain_radius**2 * site_density) * exp(-ebed * this%network%reactions(i)%gamma / Tdust)
                rdiff2 = anu2 / (4._DP * pi * grain_radius**2 * site_density) * exp(-ebed * this%network%reactions(i)%gamma / Tdust)
                akbar = exp(-this%network%reactions(i)%gamma / Tdust)
                ! Tunneling via surface sites:
                if (tunneling_hop) THEN
                    if (this%network%reactions(i)%reactant_1=='gH' .OR. this%network%reactions(i)%reactant_1=='gH2') THEN
                        amur = this%network%species_weight(this%network%reactions(i)%ir1) * mH
                        akbar = exp(-4._DP * pi / hP * 1e-8_DP * sqrt(2._DP * amur * ebed * this%network%reactions(i)%gamma * kB))
                        Rdiff1 = anu1 / (4._DP * pi * grain_radius**2 * site_density) * akbar
                    endif
                    if (this%network%reactions(i)%reactant_2=='gH' .OR. this%network%reactions(i)%reactant_2=='gH2') THEN
                        amur = this%network%species_weight(this%network%reactions(i)%ir2) * mH
                        akbar = exp(-4._DP * pi / hP * 1e-8_DP * sqrt(2._DP * amur * ebed * this%network%reactions(i)%gamma * kB))
                        Rdiff2 = anu2 / (4._DP * pi * grain_radius**2 * site_density) * akbar
                    endif
                endif

                ! Tunneling via reaction barriers:
                if (tunneling_barrier) THEN
                    if (this%network%reactions(i)%reactant_1=='gH' .OR. this%network%reactions(i)%reactant_1=='gH2' &
                            .OR. this%network%reactions(i)%reactant_2=='gH' .OR. this%network%reactions(i)%reactant_2=='gH2') THEN
                        amur = this%network%species_weight(this%network%reactions(i)%ir1) &
                                * this%network%species_weight(this%network%reactions(i)%ir2) &
                                / (this%network%species_weight(this%network%reactions(i)%ir1)&
                                        + this%network%species_weight(this%network%reactions(i)%ir2)) * mH
                        akbar = exp(-4._DP * pi / hP * 1e-8_DP * sqrt(2._DP * amur * this%network%reactions(i)%gamma * kB))
                    endif
                endif
                this%network%reactions(i)%rate = akbar * (Rdiff1 + Rdiff2) / dust_numdens / sads
                if (this%network%reactions(i)%reactant_1 == this%network%reactions(i)%reactant_2) then
                    this%network%reactions(i)%rate = this%network%reactions(i)%rate / 2._DP
                end if
                if (this%network%reactions(i)%product_1(1:1) == 'g') THEN
                    this%network%reactions(i)%rate = this%network%reactions(i)%rate * (1._DP - rdratio)
                else
                    this%network%reactions(i)%rate = this%network%reactions(i)%rate * rdratio
                endif

                !                if (Tdust>Tdust_no_surf_chem) this%network%reactions(i)%rate = 0._DP !No surface reactions if temperature is high
                !                if (Gfactor_UV>Gfact_no_surf_chem) this%network%reactions(i)%rate = 0._DP !No surface reactions if radiation is high
            CASE (14) !Photoionization of grains
                !                IF(TRIM(s(this%network%reactions(i)%ir1)%name)=='G-') then
                !                    this%network%reactions(i)%rate = grain_photoelectric_rate(-1)
                !                elseIF(TRIM(s(this%network%reactions(i)%ir1)%name)=='G0') then
                !                    this%network%reactions(i)%rate = grain_photoelectric_rate(0)
                !                else
                !                    stop "ERROR [Calculate_Rates]: unknown grain photoionization reaction"
                !                endif
                !                TODO currenty disabled
                this%network%reactions(i)%rate = 0._DP
            CASE (15) !Release of fresh ice in asteroid collisions
                this%network%reactions(i)%rate = 0._DP
            CASE DEFAULT
                stop "Unknown reaction type"
            END SELECT
            if(this%network%reactions(i)%rate<0._DP) then
                write(stderr, '(a40,2i5,a1,4a)') "No rate was specified for reaction #", i, this%network%reactions(i)%rtype, " ", &
                        this%network%reactions(i)%reactant_1, this%network%reactions(i)%reactant_2, &
                        this%network%reactions(i)%product_1, this%network%reactions(i)%product_2
                write(stderr, *) this%network%reactions(i)%alpha, this%network%reactions(i)%beta, this%network%reactions(i)%gamma, &
                        this%network%reactions(i)%rtype
            endif
            !output rates
            !            write(22,1000)this%network%reactions(i)%idx,this%network%reactions(i)%rtype,this%network%reactions(i)%reactant_1,this%network%reactions(i)%reactant_2,this%network%reactions(i)%product_1,this%network%reactions(i)%p2,this%network%reactions(i)%p3,this%network%reactions(i)%p4,this%network%reactions(i)%p5,this%network%reactions(i)%rate

        end do

    end subroutine calculate_rates


    function findindex(this, key) result(index)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        integer :: index

        index = 0
        index = findloc(param_names, key, dim = 1)
        if (index == 0) then
            write(stderr, *) trim(key), " not found!"
        end if
    end function findindex

    subroutine set_default_from_current(this)
        class(Alchemic), intent(inout) :: this
        default_values = this%param_values
    end subroutine set_default_from_current

    subroutine set_current_from_default(this)
        class(Alchemic), intent(inout) :: this
        this%param_values = default_values
        call this%read_network()
    end subroutine set_current_from_default

    subroutine set_default(this, key, value)
        class(Alchemic), intent(inout) :: this
        character(len = *), intent(in) :: key
        character(len = *), intent(in) :: value
        integer :: index
        index = this%findindex(key)
        if (index /= 0) then
            default_values(index) = value
        end if
    end subroutine set_default

    function get_default(this, key) result(value)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        character(len = parameter_length) :: value
        integer :: index
        index = this%findindex(key)
        if (index /= 0) then
            value = default_values(index)
        end if
    end function get_default

    function get(this, key) result(value)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        character(len = parameter_length) :: value
        integer :: index
        index = this%findindex(key)
        if (index /= 0) then
            value = this%param_values(index)
        end if
    end function get

    function get_real(this, key) result(value_real)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        character(len = parameter_length) :: value
        real(DP) :: value_real
        integer :: io

        value = this%get(key)
        read(value, *, iostat = io) value_real
        if (io /= 0) then
            write(stderr, *) "Reading parameter ", key, " failed."
        end if
    end function get_real

    function get_int(this, key) result(value_int)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        character(len = parameter_length) :: value
        integer :: value_int
        integer :: io

        value = this%get(key)
        read(value, *, iostat = io) value_int
        if (io /= 0) then
            write(stderr, *) "Reading parameter ", key, " failed."
        end if
    end function get_int

    function get_bool(this, key) result(value_bool)
        class(Alchemic), intent(in) :: this
        character(len = *), intent(in) :: key
        character(len = parameter_length) :: value
        logical :: value_bool
        integer :: io

        value = this%get(key)
        read(value, *, iostat = io) value_bool
        if (io /= 0) then
            write(stderr, *) "Reading parameter ", key, " failed."
        end if
    end function get_bool


    subroutine set(this, key, value)
        class(Alchemic), intent(inout) :: this
        character(len = *), intent(in) :: key
        character(len = *), intent(in) :: value
        integer :: index
        index = this%findindex(key)
        if (index /= 0) then
            this%param_values(index) = value
            if (key == "Chemical network file") then
                call this%read_network()
            end if
        end if
    end subroutine set

    subroutine from_stdin(this, prompt, stop_loop, to_default)
        class(Alchemic), intent(inout) :: this
        character(len = *), intent(in) :: prompt
        logical, intent(out), optional :: stop_loop
        logical, intent(in), optional :: to_default
        logical :: to_default_value = .false.

        character(len = parameter_name_length) :: key
        character(len = parameter_length) :: value
        character(len = parameter_length + parameter_name_length + 3) :: line
        integer :: io, split_index, ntimes, itime
        real(DP) :: tfirst, tfinish, step
        character(len = parameter_length) :: initial_network_path

        if(present(to_default)) then
            to_default_value = to_default
        end if

        initial_network_path = this%network_path
        stop_loop = .false.
        write(*, *) prompt
        do
            read(*, "(A)", iostat = io)  line
            if (io > 0) then
                write(stderr, *) 'Check input.  Something was wrong'
                exit
            else if (io < 0) then
                write(stderr, *) 'Reached EOF'
                stop_loop = .true.
                exit
            else
                if (line=="NEXT") then
                    exit
                elseif (line=="STOP") then
                    stop_loop = .true.
                    exit
                end if
                split_index = scan(line, "=")
                if (split_index == 0) then
                    write(stderr, *) "Input format: <key>=<value>. '=' not found! ", &
                            "Use 'NEXT' to finish this input, and 'STOP' or Ctrl+D to exit"
                    cycle
                end if

                key = trim(adjustl(line(1:split_index - 1)))
                value = trim(adjustl(line(split_index + 1:)))
                if (to_default_value) then
                    call this%set_default(key, value)
                else
                    call this%set(key, value)
                end if
            end if
        end do

        tfinish = this%get_real("Evolution time")
        tfirst = this%get_real("First time step")
        ntimes = this%get_int("Number of time steps")

        if (allocated(this%times)) deallocate(this%times)
        allocate(this%times(ntimes))
        this%times(1) = 0.0_DP
        this%times(2) = tfirst
        step = exp(log(tfinish/tfirst) / (ntimes-2))
        do itime = 3, ntimes-1
            this%times(itime) = this%times(itime-1) * step
        end do
        this%times(ntimes) = tfinish

        this%network_path = this%get("Chemical network file")

        if (.not. (this%network%is_read .and. (this%network_path == initial_network_path))) then
            write(*, *) "Reading network from ", this%network_path
            call this%network%read(this%network_path)
        end if
    end subroutine from_stdin

    subroutine report(this)
        class(Alchemic), intent(in) :: this
        integer :: i

        do i = 1, n_parameters
            write(*, *) param_names(i), ": ", trim(this%param_values(i))
        end do
        call this%network%report()


        call this%report_abundances()

    end subroutine report

    subroutine report_abundances(this)
        class(Alchemic), intent(in) :: this
        integer :: i, fileunit
        character(len = 30) formatstring
        !        ! TODO transpose output
        write(formatstring, *) "(A, ", this%network%number_of_species, "E12.4)"
        open(newunit = fileunit, file = "last_abundances.out")
        do i = 1, this%network%number_of_species
            write(*, formatstring) this%network%species(i), this%abundances(:, i) + 1.0E-99_DP
            write(fileunit, formatstring) this%network%species(i), this%abundances(:, i) + 1.0E-99_DP
        end do
        close(fileunit)
    end subroutine report_abundances

    subroutine report_defaults(this)
        class(Alchemic), intent(in) :: this
        integer :: i

        do i = 1, n_parameters
            write(*, *) param_names(i), ": ", trim(default_values(i))
        end do
    end subroutine report_defaults

    subroutine run(this)
        class(Alchemic), intent(inout) :: this
        write(*, *) "Processing..."
        call this%calculate_rates()
        call this%run_dvode_solver_sparse()
    end subroutine run

    subroutine run_dvode_solver_sparse(this)
        class(Alchemic), intent(inout) :: this
        type(ChemicalReaction), allocatable :: r(:)
        real(DP) :: time

        integer :: itime, ispec
        INTEGER :: istate, itask, is !, nia, nja
        REAL(DP), DIMENSION(:), ALLOCATABLE :: y, ydot, atol_vec, rtol_vec
        REAL(DP) :: tcur = 0.0_DP, tnext
        REAL(DP) :: tstart, tfinish
        REAL(DP) :: gas_density, dust_to_gas, monomer_density, grain_radius, gas_numdens, dust_numdens

        TYPE (VODE_OPTS) :: OPTIONS
        integer, dimension(:), allocatable :: idx
        real(DP), dimension(:), allocatable :: LB, UB
        integer :: nspecies, i, ntimes

        !Arrays to retrieve integration statistics after the work of DVODE is done.
        !Originally ISTATS had 31 components, 2 components were added to include indices of species contributing the most to the error.
        REAL(DP) :: RSTATS(22)
        INTEGER :: ISTATS(33)


        allocate(r(this%network%number_of_reactions))
        r = this%network%reactions
        !To save or restore DVODE variables
        ! REAL(DP) :: RSAV(49)
        ! INTEGER  :: ISAV(41),JOB

        !DVODE estimates the local error of integration. If it is < rtol*|y_i| + atol, then the solution is good.
        !Thus,  the abundance error for major species is rtol*|y| and atol for minor species.

        itask = 1
        istate = 1

!        write(*, *) tfinish - tstart, tstart, tfinish

        nspecies = this%network%number_of_species

        !        if (present(adjust_chem))    l_adjust_chem = adjust_chem
        !        if (present(chemistry_spy))  l_chemistry_spy = chemistry_spy

        if(.not.allocated(y))        allocate(y(nspecies))
        if(.not.allocated(ydot))     allocate(ydot(nspecies))
        if(.not.allocated(atol_vec)) allocate(atol_vec(nspecies))
        if(.not.allocated(rtol_vec)) allocate(rtol_vec(nspecies))
        if(.not.allocated(idx)) allocate(idx(nspecies))
        if(.not.allocated(LB)) allocate(LB(nspecies))
        if(.not.allocated(UB)) allocate(UB(nspecies))

        !write(*,*) nspecies, size(initial_abundances), size(final_abundances)

        gas_density = this%get_real("Gas density")
        dust_to_gas = this%get_real("Dust to gas mass ratio")
        monomer_density = this%get_real("Dust grain density")
        grain_radius = this%get_real("Dust radius")
        ! TODO replace 2.33 with a_mu

        gas_numdens = gas_density / mH / 2.33_DP
        dust_numdens = gas_density * dust_to_gas / (4._DP * pi * grain_radius**3 * monomer_density / 3._DP)

        where (this%network%species(:)(1:1) == "G")
            y = this%initial_abundances * dust_numdens
        elsewhere
            y = this%initial_abundances * gas_numdens
        end where
        this%abundances(1,:) = y / gas_numdens
        atol_vec = this%get_real("Absolute tolerance")

        OPTIONS = SET_OPTS(MXSTEP = 5000000, RELERR =  this%get_real("Relative tolerance"), ABSERR_VECTOR = atol_vec, &
                METHOD_FLAG = 126, MA28_RPS = .TRUE., MXHNIL = 1)

        do itime = 2, size(this%times)
            tnext = this%times(itime) * year
            write(*,*) "Time = ", tcur
            CALL DVODE_F90(rate_eqs, nspecies, y, tcur, tnext, itask, istate, OPTIONS, sparse_jac)

            do is = 1, nspecies
                if(y(is) < -1e-4_DP) then !this is relative to dust, so equivalent to x=1e-16 wrt H
                    write(*, '(a,a,e15.5)') "Too low accuracy in chemistry for  ", this%network%species(is), y(is)
                endif
            enddo
            !Request additional info about DVODE run. ISTATS(32) stores the maxloc of |ACOR| array, ISTATS(33) of |ACOR|*EWT.
            !DVODE decides on the quality of intergation based on |ACOR|*EWT, EWT=1/(RTOL*|y|+ATOL)
            CALL GET_STATS(RSTATS, ISTATS)
            !        spec2blame (ircur, izcur) = initial_abundances(ISTATS(33))%name
            !        nDVODEsteps(ircur, izcur) = ISTATS(11)
            !        call Prepare_Rates_for_GTB(nspecies, y, ircur, izcur)
            !        write(*,*) y
            this%abundances(itime,:) = y / gas_numdens
        end do

    contains

        SUBROUTINE rate_eqs(neq, t, y, ydot)
            implicit none
            !right-hand side of chemical rate equations dn_i/dt=production-destruction
            integer, intent(IN) :: neq   !number of equations = number of species (nspecies)
            real(DP), intent(IN) :: t     !current time. It is assumed that rate coefficients does not change during (tcur,tnext).
            real(DP), dimension(neq), intent(IN) :: y     !abundances of species
            real(DP), dimension(neq), intent(OUT) :: ydot  !their rates of production
            integer :: jreac !to cycle over all nreactions reactions
            real(DP) :: rr    !current reaction rate [y] s-1
            integer :: nreactions
            !            write(98,*) t,y(7)
            nreactions = size(r)
            !real(DP), parameter                   :: epslim=1e-20_DP
            ydot(:) = 0._DP
            !            write(99,*) y
            do jreac = 1, nreactions
                rr = 0._DP
                if (r(jreac)%ir2==0) then !one-body reaction (the second component is pseudo-species like PHOTON, CRP, CRPHOT, FREEZE, DESORB)
                    rr = r(jreac)%rate * y(r(jreac)%ir1) !r(jreac)%rate, s-1
                else !two-body reaction
                    rr = r(jreac)%rate * y(r(jreac)%ir1) * y(r(jreac)%ir2) !r(jreac)%rate, s-1/[y] - thus, the units of two-body rate coefficients depend on the units of abundance (currently, they are mol grain-1, may be switched to mol cm-3.
                endif
                !if(abs(rr).lt.epslim*1e-3_DP) rr=0._DP
                !Now account for all the reactants and products in their corresponding rates
                ydot(r(jreac)%ir1) = ydot(r(jreac)%ir1) - rr !First reactant (always present) destruction, thus, negative
                if (r(jreac)%ir2/=0) ydot(r(jreac)%ir2) = ydot(r(jreac)%ir2) - rr !Second reactant are absent for one-body reactions, r(jreac)%ir2 and r(jreac)%ip{2-5} = 0 if absent or in case of pseudo-species
                ydot(r(jreac)%ip1) = ydot(r(jreac)%ip1) + rr !First  product is always present
                if (r(jreac)%ip2/=0) ydot(r(jreac)%ip2) = ydot(r(jreac)%ip2) + rr !Second product, if present
                if (r(jreac)%ip3/=0) ydot(r(jreac)%ip3) = ydot(r(jreac)%ip3) + rr !Third  product, if present
                if (r(jreac)%ip4/=0) ydot(r(jreac)%ip4) = ydot(r(jreac)%ip4) + rr !Fourth product, if present
                if (r(jreac)%ip5/=0) ydot(r(jreac)%ip5) = ydot(r(jreac)%ip5) + rr !Fifth  product, if present
                !print*,jreac, r(jreac)%ir1,r(jreac)%ir2,r(jreac)%ip1,r(jreac)%ip2,r(jreac)%ip3,r(jreac)%ip4, r(jreac)%ip5
                !                if ((r(jreac)%ip1/=0).and.(r(jreac)%ip2/=0).and.(r(jreac)%ip3/=0).and.(r(jreac)%ip4/=0).and.(r(jreac)%ip5/=0)) then
                !                write(99, '(I7, 7A33, 999e33.15)') &
                !                        jreac, r(jreac)%r1, r(jreac)%r2, &
                !                        r(jreac)%p1, r(jreac)%p2, r(jreac)%p3, r(jreac)%p4, r(jreac)%p5
                !                write(99, '(A7)',advance="no"),""
                !                if (r(jreac)%ir1/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ir1)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ir2/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ir2)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ip1/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ip1)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ip2/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ip2)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ip3/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ip3)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ip4/=0) then
                !                    write(99, '(e33.15)',advance="no") ydot(r(jreac)%ip4)
                !                else
                !                    write(99, '(e33.15)',advance="no") 0.0_DP
                !                end if
                !
                !                if (r(jreac)%ip5/=0) then
                !                    write(99, '(e33.15)') ydot(r(jreac)%ip5)
                !                else
                !                    write(99, '(e33.15)') 0.0_DP
                !                end if


                !                end if ydot(r(jreac)%ir2)
                !                write(99, '(999e22.7)') &
                !                        ydot(r(jreac)%ip1), ydot(r(jreac)%ip2), ydot(r(jreac)%ip3), ydot(r(jreac)%ip4), ydot(r(jreac)%ip5)

                !                end if
            enddo
            !            stop
            !where(abs(ydot).lt.epslim) ydot=0._DP
            !write(*,*) maxval(abs(ydot)),minval(abs(ydot),mask=ydot/=0)
        END SUBROUTINE rate_eqs


        SUBROUTINE sparse_jac(neq, tcur, y, IA, JA, NZ, PD_vec)
            implicit none
            !Provides the Jacobian matrix for chemical rate equations
            !As the 2D Jacobian PD_full(1:neq,1:neq) is quite sparse, only the non-zero elements can be stored and returned as 1D array PD_vec
            !PD_full(i,j)=d rate_eqs_i/d y_j, right?
            integer, intent(in) :: neq    !number of equations = number of species (nspecies)
            real(DP), intent(in) :: tcur   !current time
            real(DP), dimension(neq), intent(in) :: y      !current solution vector (abundances)
            integer, dimension(*), intent(out) :: IA, JA !sparsity structure arrays
            integer, intent(inout) :: NZ     !number of non-zero elements
            !If NZ = 0 on input, replace NZ by the number of nonzero elements in the Jacobian. Do NOT define the arrays IA, JA, PD at this time.
            !When a call is made with NZ unequal to 0, you must define the sparsity structure arrays IA and JA, and the sparse Jacobian P.
            real(DP), dimension(*), intent(out) :: PD_vec !numerical values of the Jacobian elements (stored in 1D vector in a way coded by IA,JA)
            integer :: i, j, k, l
            integer :: nspecies, nreactions
            real(DP), dimension(:, :), allocatable :: PD_full !local non-sparse Jacobian. Only non-zero (and diagonal) elements is to be stored in PD_vec
            nspecies = size(y)
            nreactions = size(r)
            allocate(PD_full(nspecies, nspecies))
            PD_full(:, :) = 0._DP

            do j = 1, nreactions
                if (r(j)%ir2/=0) THEN !two-body reactions
                    PD_full(r(j)%ir1, r(j)%ir1) = PD_full(r(j)%ir1, r(j)%ir1) - r(j)%rate * y(r(j)%ir2)
                    PD_full(r(j)%ir2, r(j)%ir1) = PD_full(r(j)%ir2, r(j)%ir1) - r(j)%rate * y(r(j)%ir2)
                    PD_full(r(j)%ip1, r(j)%ir1) = PD_full(r(j)%ip1, r(j)%ir1) + r(j)%rate * y(r(j)%ir2)
                    if (r(j)%ip2/=0) PD_full(r(j)%ip2, r(j)%ir1) = PD_full(r(j)%ip2, r(j)%ir1) + r(j)%rate * y(r(j)%ir2)
                    if (r(j)%ip3/=0) PD_full(r(j)%ip3, r(j)%ir1) = PD_full(r(j)%ip3, r(j)%ir1) + r(j)%rate * y(r(j)%ir2)
                    if (r(j)%ip4/=0) PD_full(r(j)%ip4, r(j)%ir1) = PD_full(r(j)%ip4, r(j)%ir1) + r(j)%rate * y(r(j)%ir2)
                    if (r(j)%ip5/=0) PD_full(r(j)%ip5, r(j)%ir1) = PD_full(r(j)%ip5, r(j)%ir1) + r(j)%rate * y(r(j)%ir2)

                    PD_full(r(j)%ir1, r(j)%ir2) = PD_full(r(j)%ir1, r(j)%ir2) - r(j)%rate * y(r(j)%ir1)
                    PD_full(r(j)%ir2, r(j)%ir2) = PD_full(r(j)%ir2, r(j)%ir2) - r(j)%rate * y(r(j)%ir1)
                    PD_full(r(j)%ip1, r(j)%ir2) = PD_full(r(j)%ip1, r(j)%ir2) + r(j)%rate * y(r(j)%ir1)
                    if (r(j)%ip2/=0) PD_full(r(j)%ip2, r(j)%ir2) = PD_full(r(j)%ip2, r(j)%ir2) + r(j)%rate * y(r(j)%ir1)
                    if (r(j)%ip3/=0) PD_full(r(j)%ip3, r(j)%ir2) = PD_full(r(j)%ip3, r(j)%ir2) + r(j)%rate * y(r(j)%ir1)
                    if (r(j)%ip4/=0) PD_full(r(j)%ip4, r(j)%ir2) = PD_full(r(j)%ip4, r(j)%ir2) + r(j)%rate * y(r(j)%ir1)
                    if (r(j)%ip5/=0) PD_full(r(j)%ip5, r(j)%ir2) = PD_full(r(j)%ip5, r(j)%ir2) + r(j)%rate * y(r(j)%ir1)
                else !one-body reactions (the second component is pseudo-species like PHOTON, CRP, CRPHOT, FREEZE, DESORB)
                    PD_full(r(j)%ir1, r(j)%ir1) = PD_full(r(j)%ir1, r(j)%ir1) - r(j)%rate
                    PD_full(r(j)%ip1, r(j)%ir1) = PD_full(r(j)%ip1, r(j)%ir1) + r(j)%rate
                    if (r(j)%ip2/=0) PD_full(r(j)%ip2, r(j)%ir1) = PD_full(r(j)%ip2, r(j)%ir1) + r(j)%rate
                    if (r(j)%ip3/=0) PD_full(r(j)%ip3, r(j)%ir1) = PD_full(r(j)%ip3, r(j)%ir1) + r(j)%rate
                    if (r(j)%ip4/=0) PD_full(r(j)%ip4, r(j)%ir1) = PD_full(r(j)%ip4, r(j)%ir1) + r(j)%rate
                    if (r(j)%ip5/=0) PD_full(r(j)%ip5, r(j)%ir1) = PD_full(r(j)%ip5, r(j)%ir1) + r(j)%rate
                endif
            enddo

            k = 0
            do i = 1, neq
                do j = 1, neq
                    if (i==j .OR. PD_full(i, j)/=0._DP) k = k + 1
                enddo
            enddo

            if (NZ==0) THEN
                NZ = k
            else
                IA(1) = 1
                k = 0
                l = 0

                do i = 1, neq
                    l = 0
                    do j = 1, neq
                        if (i==j .OR. PD_full(j, i)/=0._DP) THEN
                            k = k + 1
                            l = l + 1
                            JA(k) = j
                            PD_vec(k) = PD_full(j, i)
                        endif
                    enddo
                    IA(i + 1) = IA(i) + l
                enddo
            endif
            deallocate(PD_full)
        END SUBROUTINE sparse_jac


    END SUBROUTINE run_dvode_solver_sparse


    subroutine read_network(this)
        class(Alchemic), intent(inout) :: this
        this%network_path = this%get("Chemical network file")
        write(*, *) "Reading network from ", this%network_path
        call this%network%read(this%network_path)
        call this%read_initial_abundances()
    end subroutine read_network

    subroutine read_initial_abundances(this)
        class(Alchemic), intent(inout) :: this
        integer :: fileunit, initial_abundances_to_read, i
        character(len = species_name_length), allocatable :: initial_species(:)
        real(DP), allocatable :: initial_abundances(:)

        if (allocated(this%initial_abundances)) deallocate(this%initial_abundances)
        allocate(this%initial_abundances(this%network%number_of_species))
        this%initial_abundances = 0.0_DP

        ! TODO this requires specific order of the input parameters!
        if (allocated(this%abundances)) deallocate(this%abundances)
        allocate(this%abundances(&
                this%get_int("Number of time steps"), &
                this%network%number_of_species))
        this%abundances = 0.0_DP

        open(newunit = fileunit, file = this%get("Initial abundance file"))
        read(fileunit, *)
        read(fileunit, *) initial_abundances_to_read
        allocate(initial_species(initial_abundances_to_read))
        allocate(initial_abundances(initial_abundances_to_read))

        do i = 1, initial_abundances_to_read
            read(fileunit, *) initial_species(i), initial_abundances(i)
        end do
        close(fileunit)

        do i = 1, initial_abundances_to_read
            this%initial_abundances(findloc(this%network%species, initial_species(i))) &
                    = initial_abundances(i)
        end do
        this%abundances(1, :) = this%initial_abundances(:)
    end subroutine read_initial_abundances


end module class_Alchemic


program server
    use class_Alchemic, only : Alchemic, alchemic_parameter_length => parameter_length
    use class_ChemicalNetwork, only : ChemicalNetwork
    implicit none
    integer :: io, x, sum, split_index, number_of_eofs
    character(len = alchemic_parameter_length) :: linea, lineb
    character(len = alchemic_parameter_length * 2) :: line
    type(Alchemic) :: alch
    logical :: stop_loop = .false.

    sum = 0
    number_of_eofs = 0

!    call alch%report()
!    write(*, *)"================================================================="
!    call alch%set_current_from_default()
!    call alch%report()

    do
        call alch%set_current_from_default()
        call alch%from_stdin("Give your current input", stop_loop)
        if (stop_loop) then
            exit
        end if
        call alch%run()
        call alch%report()
        call alch%report_abundances()
        write (*,*) alch%times
    end do
end program server