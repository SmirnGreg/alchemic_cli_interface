# Interface

Prototype for Alchemic CLI

## Usage

Compile first:

```bash
gfortran server.f90
```

### CLI interface

Run generated executable:

```bash
./a.out
```

After prompted for a default argument dictionary, type `key=value`, one per line, for each of the parameters.
Currently, `a`, `b` and `c` keys are hard-coded. Order is not relevant, and the last entry per key is remembered. When
finished, print `STOP`.

```
a=10
c=3
b=2
a=1
STOP
```

You will get result:

```
Processing...
a=1
b=2
c=3
```

And prompted for a new set of key-value pairs. Now, you can only specify keys which differ from the default dictionary
passed before:

```
b=10
STOP
```

```
Processing...
a=1
b=10
c=3 
```

```
a=15
STOP
```

```
Processing...
a=15
b=2
c=3 
```

Note that the default was not overridden after the first input.

To finish, pass end-of-file (Ctrl+D).

### Pipe

```bash
./a.out < in > out
diff out test_out 
```

### Python `pexpect`

```bash
python3 caller.py
```
