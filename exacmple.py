from matplotlib import pyplot as plt
from pandas import DataFrame

# cmap = get_cmap("tab20", 20)

df = DataFrame()
with open("last_abundances.out", "r") as fff:
    for line in fff.readlines():
        species, *abundances = line.split()
        df[species] = [*map(float, abundances)]

df.plot(
    y=["CO", "gCO", "C", "gC", "O", "gO", "e-", "H2O", "gH2O"],
    logy=True,
    # colormap="tab20"
    ylim=[1e-15,1]
)
plt.show()