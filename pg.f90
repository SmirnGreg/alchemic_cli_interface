program playground
    implicit none
    character(10) :: ac = "TRUE"
    character(10) :: bc = "FALSE"
    logical a, b
    read(ac, *) a
    read(bc, *) b
    if (a) write(*,*) "true is true"
    if (.not. b) write(*,*) "false is false"

end program playground